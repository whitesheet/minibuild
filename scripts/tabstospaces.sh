#!/bin/bash

#=================================================================================================
#
#   Compiler independent safe bit-fields for C++98
#
#   File:          /scripts/tabs to spaces.cpp
#
#   Description:   This script coverts tabs to spaces on source and header files found
#                  recursively inside the current working directory.
#
#   License:       Copyright (c) 2015 Julian Dessecker
#
#                  Permission is hereby granted, free of charge, to any person obtaining
#                  a copy of this software and associated documentation files
#                  (the "Software"), to deal in the Software without restriction,
#                  including without limitation the rights to use, copy, modify, merge,
#                  publish, distribute, sublicense, and/or sell copies of the Software,
#                  and to permit persons to whom the Software is furnished to do so,
#                  subject to the following conditions:
#
#                  The above copyright notice and this permission notice shall be
#                  included in all copies or substantial portions of the Software.
#
#                  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
#                  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
#                  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
#                  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
#                  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
#                  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#                  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#                  SOFTWARE.
#
#=================================================================================================


find . -name '*.h' ! -type d -exec bash -c 'expand -t 4 "$0" | sed $"s/\r$//" > /tmp/e && mv /tmp/e "$0"' {} \;
find . -name '*.sh' ! -type d -exec bash -c 'expand -t 4 "$0" | sed $"s/\r$//" > /tmp/e && mv /tmp/e "$0"; chmod +x "$0"' {} \;
find . -name '*.hpp' ! -type d -exec bash -c 'expand -t 4 "$0" | sed $"s/\r$//" > /tmp/e && mv /tmp/e "$0"' {} \;
find . -name '*.c' ! -type d -exec bash -c 'expand -t 4 "$0" | sed $"s/\r$//" > /tmp/e && mv /tmp/e "$0"' {} \;
find . -name '*.cc' ! -type d -exec bash -c 'expand -t 4 "$0" | sed $"s/\r$//" > /tmp/e && mv /tmp/e "$0"' {} \;
find . -name '*.cpp' ! -type d -exec bash -c 'expand -t 4 "$0" | sed $"s/\r$//" > /tmp/e && mv /tmp/e "$0"' {} \;
find . -name '*.lua' ! -type d -exec bash -c 'expand -t 4 "$0" | sed $"s/\r$//" > /tmp/e && mv /tmp/e "$0"' {} \;
find . -name '*.bat' ! -type d -exec bash -c 'cat "$0" | sed $"s/\r$//" > /tmp/e && mv /tmp/e "$0"' {} \;
find . -name '*.ps1' ! -type d -exec bash -c 'cat "$0" | sed $"s/\r$//" > /tmp/e && mv /tmp/e "$0"' {} \;
find . -name '*.s' ! -type d -exec bash -c 'expand -t 4 "$0" | sed $"s/\r$//" > /tmp/e && mv /tmp/e "$0"' {} \;
