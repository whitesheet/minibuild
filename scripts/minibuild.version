#!/bin/bash

#===============================================================================
#
#    minibuild, a minimal bare-metal toolchain
#
#    License:       Copyright (c) 2018 Julian Dessecker
#
#                   Permission is hereby granted, free of charge, to any person
# 					obtaining a copy of this software and associated
# 					documentation files (the "Software"), to deal in the
# 					Software without restriction, including without limitation
# 					the rights to use, copy, modify, merge, publish, distribute,
#                   sublicense, and/or sell copies of the Software, and to
# 					permit persons to whom the Software is furnished to do so,
#                   subject to the following conditions:
#
#                   The above copyright notice and this permission notice shall
# 					be included in all copies or substantial portions of the
# 					Software.
#
#                   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
# 					KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# 					WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
# 					PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
# 					OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# 					OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
# 					OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
# 					THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
#===============================================================================

RPROOT=`dirname $0` 2> /dev/null
RPROOT=`readlink -f $RPROOT` 2> /dev/null

pushd $RPROOT 1> /dev/null 2> /dev/null
GIT_TAG_VERSION=`git describe --abbrev=0 --tags`
GIT_REVISION=`git rev-list $GIT_TAG_VERSION.. --count`
GIT_VERSION="$GIT_TAG_VERSION.$GIT_REVISION"

GIT_LOCAL_MODIFIED=`git diff`
if [ "x$GIT_LOCAL_MODIFIED" != "x" ]; then
	GIT_LOCAL_MODIFIED="-modified"
fi

echo "${GIT_VERSION}${GIT_LOCAL_MODIFIED}"
popd 1> /dev/null 2> /dev/null