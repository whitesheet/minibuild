#===============================================================================
#
#    minibuild, a minimal bare-metal toolchain
#
#    License:       Copyright (c) 2018 Julian Dessecker
#
#                   Permission is hereby granted, free of charge, to any person
# 					obtaining a copy of this software and associated
# 					documentation files (the "Software"), to deal in the
# 					Software without restriction, including without limitation
# 					the rights to use, copy, modify, merge, publish, distribute,
#                   sublicense, and/or sell copies of the Software, and to
# 					permit persons to whom the Software is furnished to do so,
#                   subject to the following conditions:
#
#                   The above copyright notice and this permission notice shall
# 					be included in all copies or substantial portions of the
# 					Software.
#
#                   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
# 					KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# 					WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
# 					PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
# 					OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# 					OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
# 					OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
# 					THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
#===============================================================================


cmake_minimum_required(VERSION 3.12)

# setup system
SET(CMAKE_SYSTEM_NAME Generic)
SET(CMAKE_SYSTEM_VERSION 1)

# setup minibuild module search path
set (CMAKE_MODULE_PATH $ENV{CMAKE_MODULE_PATH})

# enable assembly language
enable_language (ASM)

# force compiler config
set (CMAKE_C_COMPILER_WORKS 1)
set (CMAKE_CXX_COMPILER_WORKS 1)
set (CMAKE_C_COMPILER_FORCED)
set (CMAKE_CXX_COMPILER_FORCED)

# include toolchain and target config
include (minibuild-toolchain)
include (minibuild-target)

# setup startup files
set (StartupSources $ENV{MINIBUILD_STARTUP})

# check size of executable
macro (check_executable_size executable_name)
	add_custom_command (
		TARGET ${executable_name}
	    POST_BUILD
	    COMMAND minibuild.show_size $<TARGET_FILE:${executable_name}>
	)
endmacro (check_executable_size)

# initialize modules
include (minibuild-install-modules)
