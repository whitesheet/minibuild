//==============================================================================
//
//   minibuild, a minimal bare-metal toolchain
//
//   License:       Copyright (c) 2018 Julian Dessecker
//
//                  Permission is hereby granted, free of charge, to any person
//                  obtaining a copy of this software and associated
//                  documentation files (the "Software"), to deal in the
//                  Software without restriction, including without limitation
//                  the rights to use, copy, modify, merge, publish, distribute,
//                  sublicense, and/or sell copies of the Software, and to
//                  permit persons to whom the Software is furnished to do so,
//                  subject to the following conditions:
//
//                  The above copyright notice and this permission notice shall
//                  be included in all copies or substantial portions of the
//                  Software.
//
//                  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
//                  KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//                  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//                  PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
//                  OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
//                  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
//                  OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
//                  THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//==============================================================================


#ifndef __HARDWARE_CORTEX_M4_INCLUDED__
#define __HARDWARE_CORTEX_M4_INCLUDED__


#ifdef EXTERN_C
#   undef EXTERN_C
#endif
#ifdef __cplusplus
#   define EXTERN_C extern "C"
#else
#   define EXTERN_C
#endif


//------------------------------------------------------------------------------
// define target
//------------------------------------------------------------------------------
#define _TARGET_ARM 1
#define _TARGET_CORTEX 1
#define _TARGET_CORTEX_M4 1


//------------------------------------------------------------------------------
// disable interrupts
//------------------------------------------------------------------------------
#if TOOLCHAIN_SUPPORTS_INLINE_ASM == 0
    EXTERN_C void _disable_interrupts ();
#else
    EXTERN_C inline void __attribute__ ((always_inline)) _disable_interrupts ()
    {
        asm volatile
        (
            "cpsid if"
            :
            :
            :
        );
    }
#endif


//------------------------------------------------------------------------------
// enable interrupts
//------------------------------------------------------------------------------
#if TOOLCHAIN_SUPPORTS_INLINE_ASM == 0
    EXTERN_C void _enable_interrupts ();
#else
    EXTERN_C inline void __attribute__ ((always_inline)) _enable_interrupts ()
    {
        asm volatile
        (
            "cpsie if"
            :
            :
            :
        );
    }
#endif


//------------------------------------------------------------------------------
// issue memory barrier
//------------------------------------------------------------------------------
#if TOOLCHAIN_SUPPORTS_INLINE_ASM == 0
    EXTERN_C void _memory_barrier ();
#else
    EXTERN_C inline void __attribute__ ((always_inline)) _memory_barrier ()
    {
        asm volatile
        (
            "dsb"
            :
            :
            : "memory"
        );
    }
#endif


//------------------------------------------------------------------------------
// execute break point instruction
//------------------------------------------------------------------------------
#if TOOLCHAIN_SUPPORTS_INLINE_ASM == 0
    EXTERN_C void _issue_break_point ();
#else
    EXTERN_C inline void __attribute__ ((always_inline)) _issue_break_point ()
    {
        asm volatile
        (
            "bkpt 0"
            :
            :
            :
        );
    }
#endif


//------------------------------------------------------------------------------
// write zero terminated string to trace output (weak)
//------------------------------------------------------------------------------
EXTERN_C void __attribute__((weak)) _trace_write_str (const char* ptr);


//------------------------------------------------------------------------------
// invoke semi hosting call
//------------------------------------------------------------------------------
#if TOOLCHAIN_SUPPORTS_INLINE_ASM == 0
    EXTERN_C int _call_host (int reason, void* argument);
#else
    EXTERN_C inline int __attribute__ ((always_inline)) _call_host (int reason, void* arg)
    {
        int value;
        asm volatile
        (
            "mov r0, %[rsn] \n"
            "mov r1, %[arg] \n"
            "bkpt %[swi] \n"
            "mov %[val], r0"
            : [val] "=r" (value)
            : [rsn] "r" (reason), [arg] "r" (arg), [swi] "i" (0xAB)
            : "r0", "r1", "r2", "r3", "ip", "lr", "memory", "cc"
        );
        return value;
    }
#endif


//------------------------------------------------------------------------------
// write to trace log (zero terminated string)
//------------------------------------------------------------------------------
void __attribute__((weak)) _trace_write_str (const char* ptr)
{
    // write zero terminated string via semi hosting
    _call_host (0x04, (void*) ptr);
}


#endif // __HARDWARE__CORTEX_M4_INCLUDED__
