//==============================================================================
//
//   minibuild, a minimal bare-metal toolchain
//
//   License:       Copyright (c) 2018 Julian Dessecker
//
//                  Permission is hereby granted, free of charge, to any person
//                  obtaining a copy of this software and associated
//                  documentation files (the "Software"), to deal in the
//                  Software without restriction, including without limitation
//                  the rights to use, copy, modify, merge, publish, distribute,
//                  sublicense, and/or sell copies of the Software, and to
//                  permit persons to whom the Software is furnished to do so,
//                  subject to the following conditions:
//
//                  The above copyright notice and this permission notice shall
//                  be included in all copies or substantial portions of the
//                  Software.
//
//                  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
//                  KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//                  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//                  PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
//                  OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
//                  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
//                  OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
//                  THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//==============================================================================


#include <core-hw.h>
#include <stdint.h>
#include <core-clib.h>


#if (defined(MODULE_HEAP_USED) && (MODULE_HEAP_USED != 0))
#   include <heap.h>
#endif


extern "C" {


//------------------------------------------------------------------------------
// main entry point
//------------------------------------------------------------------------------
#if TOOLCHAIN_SUPPORTS_INLINE_ASM
void __attribute__((naked)) Reset_Handler ()
{
    __asm volatile
    (
        "LDR R0,=_start\n"
        "BX R0"
    );
}
#endif


//------------------------------------------------------------------------------
// default heap implementation
//------------------------------------------------------------------------------
#if MODULE_HEAP_USED
    struct __heap_t* __default_heap = 0;
#endif


//------------------------------------------------------------------------------
// forward: c/c++ runtime forwards
//------------------------------------------------------------------------------
extern void _exit (int);
extern int main ();


//------------------------------------------------------------------------------
// forward: linker constants
//------------------------------------------------------------------------------
extern unsigned int __bss_start__;
extern unsigned int __bss_end__;
extern unsigned int __ccm_bss_start__;
extern unsigned int __ccm_bss_end__;
extern unsigned int _sidata;
extern unsigned int _sdata;
extern unsigned int _edata;
extern unsigned int _si_ccmdata;
extern unsigned int _s_ccmdata;
extern unsigned int _e_ccmdata;
extern unsigned int __snj_flash__;
extern unsigned int _Heap_Begin;
extern unsigned int _Heap_Limit;
extern unsigned int _estack;


//------------------------------------------------------------------------------
// dummy wait function
//------------------------------------------------------------------------------
void _dummy_void_func ()
{
}


//------------------------------------------------------------------------------
// init .bss memory
//------------------------------------------------------------------------------
void _init_bss (unsigned int* start, unsigned int* end)
{
    unsigned int* p = start;
    while (p != end)
    {
        *(p++) = 0;
    }
}


//------------------------------------------------------------------------------
// init .data memory
//------------------------------------------------------------------------------
void _init_data (unsigned int* initdata, unsigned int* start, unsigned int* end)
{
    unsigned int* p = start;
    unsigned int* i = initdata;
    while (p != end)
    {
        *(p++) = *(i++);
    }
}


//------------------------------------------------------------------------------
// run c / c++ initializers
//------------------------------------------------------------------------------
extern void (*__preinit_array_start[]) (void) __attribute__((weak));
extern void (*__preinit_array_end[]) (void) __attribute__((weak));
extern void (*__init_array_start[]) (void) __attribute__((weak));
extern void (*__init_array_end[]) (void) __attribute__((weak));


//------------------------------------------------------------------------------
// run C/C++ initializer arrays
//------------------------------------------------------------------------------
inline void __attribute__((always_inline)) _run_init_array (void)
{
    int count = __preinit_array_end - __preinit_array_start;
    for (int i = 0; i < count; i++)
    {
        __preinit_array_start[i] ();
    }
    count = __init_array_end - __init_array_start;
    for (int i = 0; i < count; i++)
    {
        __init_array_start[i] ();
    }
}


//------------------------------------------------------------------------------
// heap pointer for
//------------------------------------------------------------------------------
#if (!defined(MODULE_HEAP_USED) || (MODULE_HEAP_USED == 0))
    int* _Heap_Pointer;
#endif


//------------------------------------------------------------------------------
// early startup code (weak dummy implementation)
//------------------------------------------------------------------------------
void __attribute__((weak, alias("_dummy_void_func"))) _early_init ();


//------------------------------------------------------------------------------
// initialize hook for interrupt manager
//------------------------------------------------------------------------------
void __attribute__((weak, alias ("_dummy_void_func"))) _irqmanager_init ();


//------------------------------------------------------------------------------
// initialize hook for trace output
//------------------------------------------------------------------------------
void __attribute__((weak, alias ("_dummy_void_func"))) _trace_init ();


//------------------------------------------------------------------------------
// initialize hook for trace output
//------------------------------------------------------------------------------
void __attribute__((weak, alias ("_dummy_void_func"))) _rtos_init ();


//------------------------------------------------------------------------------
// init c/c++ runtime
//------------------------------------------------------------------------------
void __attribute__((weak, alias("_dummy_void_func"))) _crt_init ();


//------------------------------------------------------------------------------
// startup system and jump into main function
//------------------------------------------------------------------------------
void __attribute__((noreturn,naked,noreturn)) _start ()
{
    // set interrupt vector table
    *((volatile uint32_t*) (void*) 0xE000ED08) = (uint32_t) (void*) &__snj_flash__;

    // enable FPU?
#   if CORTEX_M4_USES_FPU
        *((volatile uint32_t*) (void*) 0xE000ED88) |= 0xF << 20;
        __asm volatile ("DSB\nISB\n");
#   endif

    // init .bss segment
    _init_bss (&__bss_start__, &__bss_end__);

    // init ccm .bss segment
    _init_bss (&__ccm_bss_start__, &__ccm_bss_end__);

    // execute early initializations
    _early_init ();

    // initialize interrupt manager
    _irqmanager_init ();

    // init trace interface
    _trace_init ();

    // initialize rtos
    _rtos_init ();

    // initialize heap?
#   if (!defined(MODULE_HEAP_USED) || (MODULE_HEAP_USED == 0))
    // initialize default heap
    _Heap_Pointer = (int*) &_Heap_Begin;
#   else
    // initialize external heap module
        __default_heap = __heap_init (&_Heap_Begin, &_Heap_Limit);
        __reent_struct_main.heap = __default_heap;
#   endif

    // init .data segment
    _init_data (&_sidata, &_sdata, &_edata);

    // init .data segment of CCM
    _init_data (&_si_ccmdata, &_s_ccmdata, &_e_ccmdata);

    // init c-runtime
    _crt_init ();

    // run c / c++ initializers
    _run_init_array ();

    // call main function
    int retval = main ();
    while (1);
}


} // extern "C"
