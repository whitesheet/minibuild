//==============================================================================
//
//   minibuild, a minimal bare-metal toolchain
//
//   License:       Copyright (c) 2018 Julian Dessecker
//
//                  Permission is hereby granted, free of charge, to any person
//                  obtaining a copy of this software and associated
//                  documentation files (the "Software"), to deal in the
//                  Software without restriction, including without limitation
//                  the rights to use, copy, modify, merge, publish, distribute,
//                  sublicense, and/or sell copies of the Software, and to
//                  permit persons to whom the Software is furnished to do so,
//                  subject to the following conditions:
//
//                  The above copyright notice and this permission notice shall
//                  be included in all copies or substantial portions of the
//                  Software.
//
//                  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
//                  KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//                  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//                  PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
//                  OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
//                  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
//                  OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
//                  THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//==============================================================================


//------------------------------------------------------------------------------
// this startup file goes to the text segment
//------------------------------------------------------------------------------
.text



//------------------------------------------------------------------------------
// reset handler function
//
// void Reset_Handler ();
//------------------------------------------------------------------------------
.thumb_func
.global Reset_Handler
.extern _start
Reset_Handler:
    ldr r0, =_start
    bx r0


.if TOOLCHAIN_SUPPORTS_INLINE_ASM == 0


//------------------------------------------------------------------------------
// break point instruction
//
// void _issue_break_point ();
//------------------------------------------------------------------------------
.global _issue_break_point
_issue_break_point:
    bkpt 0
    bx %lr


//------------------------------------------------------------------------------
// call host (arm semihosting)
//
// int _call_host (int reason, void* argument);
//------------------------------------------------------------------------------
.ifdef MODULE_DBGPRINT_USED
.global _call_host
.type _call_host,%function
_call_host:
    bkpt #0xAB
    bx lr
.endif


//------------------------------------------------------------------------------
// disable interrupts
//
// void _disable_interrupts ();
//------------------------------------------------------------------------------
.global _disable_interrupts
.type _disable_interrupts,%function
_disable_interrupts:
    cpsid if
    bx lr


//------------------------------------------------------------------------------
// enable interrupts
//
// void _enable_interrupts ();
//------------------------------------------------------------------------------
.global _enable_interrupts
.type _enable_interrupts,%function
_enable_interrupts:
    cpsie if
    bx lr


//------------------------------------------------------------------------------
// issue memory barrier
//
// void _memory_barrier ();
//------------------------------------------------------------------------------
.global _memory_barrier
.type _memory_barrier,%function
_memory_barrier:
    dsb
    bx lr


.endif // TOOLCHAIN_SUPPORTS_INLINE_ASM
