#===============================================================================
#
#    minibuild, a minimal bare-metal toolchain
#
#    License:       Copyright (c) 2018 Julian Dessecker
#
#                   Permission is hereby granted, free of charge, to any person
# 					obtaining a copy of this software and associated
# 					documentation files (the "Software"), to deal in the
# 					Software without restriction, including without limitation
# 					the rights to use, copy, modify, merge, publish, distribute,
#                   sublicense, and/or sell copies of the Software, and to
# 					permit persons to whom the Software is furnished to do so,
#                   subject to the following conditions:
#
#                   The above copyright notice and this permission notice shall
# 					be included in all copies or substantial portions of the
# 					Software.
#
#                   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
# 					KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# 					WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
# 					PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
# 					OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# 					OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
# 					OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
# 					THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
#===============================================================================

cmake_minimum_required(VERSION 3.12)


# do not use LTO for system startup files
# set_source_files_properties ($ENV{MINIBUILD_ROOT}/content/target/STM32F407-v2/source/hardware.cpp PROPERTIES COMPILE_FLAGS -fno-lto)
