//==============================================================================
//
//   minibuild, a minimal bare-metal toolchain
//
//   License:       Copyright (c) 2018 Julian Dessecker
//
//                  Permission is hereby granted, free of charge, to any person
//                  obtaining a copy of this software and associated
//                  documentation files (the "Software"), to deal in the
//                  Software without restriction, including without limitation
//                  the rights to use, copy, modify, merge, publish, distribute,
//                  sublicense, and/or sell copies of the Software, and to
//                  permit persons to whom the Software is furnished to do so,
//                  subject to the following conditions:
//
//                  The above copyright notice and this permission notice shall
//                  be included in all copies or substantial portions of the
//                  Software.
//
//                  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
//                  KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//                  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//                  PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
//                  OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
//                  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
//                  OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
//                  THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//==============================================================================


#ifndef __HARDWARE_INCLUDED__
#define __HARDWARE_INCLUDED__


// include cortex m4 headers
#include "core-hw-cortex_m4.h"


// define target
#define _TARGET_CPU "STM32F407"
#define _TARGET_STM32 1
#define _TARGET_STM32F4xx 1
#define _TARGET_STM32F407 1


// calculate main stack size
#ifndef MAIN_STACK_SIZE
#   define MAIN_STACK_SIZE 1024
#elif MAIN_STACK_SIZE < 4
#   undef MAIN_STACK_SIZE
#   define MAIN_STACK_SIZE 4
#endif


#endif // __HARDWARE_INCLUDED__