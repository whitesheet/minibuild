//==============================================================================
//
//   minibuild, a minimal bare-metal toolchain
//
//   License:       Copyright (c) 2018 Julian Dessecker
//
//                  Permission is hereby granted, free of charge, to any person
//                  obtaining a copy of this software and associated
//                  documentation files (the "Software"), to deal in the
//                  Software without restriction, including without limitation
//                  the rights to use, copy, modify, merge, publish, distribute,
//                  sublicense, and/or sell copies of the Software, and to
//                  permit persons to whom the Software is furnished to do so,
//                  subject to the following conditions:
//
//                  The above copyright notice and this permission notice shall
//                  be included in all copies or substantial portions of the
//                  Software.
//
//                  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
//                  KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//                  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//                  PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
//                  OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
//                  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
//                  OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
//                  THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//==============================================================================


extern "C" {


#include<core-clib.h>
#include<errno.h>
#include<sys/stat.h>
#include<sys/time.h>
#include<sys/times.h>
#include<sys/reent.h>
#include<string.h>
#include<core-hw.h>


//------------------------------------------------------------------------------
// default output target file
//------------------------------------------------------------------------------
// place into .bss, need to be zero initialized
#ifndef USE_DEFAULT_STD_FILES
    struct __sFILE_fake __sf_default_file;
#endif


//------------------------------------------------------------------------------
// init reentrant thread local structure
//------------------------------------------------------------------------------
void __reent_init (struct __newlib_reent_t* data)
{
    // set default file handles
#ifdef USE_DEFAULT_STD_FILES
    data->reent._stdout = (__FILE*) &__sf_fake_stdin;
    data->reent._stderr = (__FILE*) &__sf_fake_stdout;
    data->reent._stderr = (__FILE*) &__sf_fake_stderr;
#else
    data->reent._stdout = (__FILE*) &__sf_default_file;
    data->reent._stderr = (__FILE*) &__sf_default_file;
    data->reent._stderr = (__FILE*) &__sf_default_file;
#endif

#ifndef _REENT_GLOBAL_ATEXIT
#   error expected global atexit
#endif

#if MODULE_HEAP_USED
    data->heap = __default_heap;
#endif
}


//------------------------------------------------------------------------------
// initialize reentrant structure (wrapper for RTOS)
//------------------------------------------------------------------------------
void _init_reentrant (void* ptr)
{
    __reent_init ((struct __newlib_reent_t*) ptr);
}


//------------------------------------------------------------------------------
// destroy reentrant structure (wrapper for RTOS)
//------------------------------------------------------------------------------
void _destroy_reentrant (void* ptr)
{
}


//------------------------------------------------------------------------------
// implement reentrant data structure
//------------------------------------------------------------------------------
static struct __newlib_reent_t __reent_struct_main;


//------------------------------------------------------------------------------
// init C runtime
//------------------------------------------------------------------------------
void _crt_init ()
{
    __reent_init (&__reent_struct_main);
    __reent_set (&__reent_struct_main);
}


//------------------------------------------------------------------------------
// assertion failed handler
//------------------------------------------------------------------------------
void _assert_failed (const char* condition, const char* file, const char* line)
{
    _trace_write_str ("Assertion ");
    _trace_write_str (condition);
    _trace_write_str (" failed in file ");
    _trace_write_str (file);
    _trace_write_str (":");
    _trace_write_str (line);
    _trace_write_str ("\n");
    _issue_break_point ();
}

} // extern "C"
