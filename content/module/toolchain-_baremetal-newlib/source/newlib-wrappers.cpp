//==============================================================================
//
//   minibuild, a minimal bare-metal toolchain
//
//   License:       Copyright (c) 2018 Julian Dessecker
//
//                  Permission is hereby granted, free of charge, to any person
//                  obtaining a copy of this software and associated
//                  documentation files (the "Software"), to deal in the
//                  Software without restriction, including without limitation
//                  the rights to use, copy, modify, merge, publish, distribute,
//                  sublicense, and/or sell copies of the Software, and to
//                  permit persons to whom the Software is furnished to do so,
//                  subject to the following conditions:
//
//                  The above copyright notice and this permission notice shall
//                  be included in all copies or substantial portions of the
//                  Software.
//
//                  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
//                  KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//                  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//                  PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
//                  OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
//                  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
//                  OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
//                  THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//==============================================================================


extern "C" {

#include<core-clib.h>
#include<errno.h>
#include<sys/stat.h>
#include<sys/time.h>
#include<sys/times.h>
#include<sys/reent.h>
#include<string.h>
#include<core-hw.h>


// forward to issue breakopint
void _issue_break_point ();


//------------------------------------------------------------------------------
// default heap implementation
//------------------------------------------------------------------------------
#if MODULE_HEAP_USED
extern struct __HeapT* __default_heap;
#endif


//------------------------------------------------------------------------------
// reentrant at exit
//------------------------------------------------------------------------------
void __attribute((weak)) __at_exit_none ()
{
}


//------------------------------------------------------------------------------
// default implementation for _exit () handler
//------------------------------------------------------------------------------
void _exit (int code);
void __attribute__((weak,noreturn)) _exit (int code __attribute__((unused)))
{
#if (defined (DEBUG) && (DEBUG != 0))
    if (code != 0)
    {
        _trace_write_str ("FATAL: abort () called.\n");
        _issue_break_point ();
    }
#endif
    while (1);
}


//------------------------------------------------------------------------------
// default implementation for abort () handler
//------------------------------------------------------------------------------
void __attribute__((weak,noreturn)) abort (void)
{
#if (defined (DEBUG) && (DEBUG != 0))
    _trace_write_str ("FATAL: abort () called.\n");
    _issue_break_point ();
#endif
    while (1);
}


//------------------------------------------------------------------------------
// default heap functions
//------------------------------------------------------------------------------

void* __nonimplemented_mem_cmd ()
{
#if (defined (DEBUG) && (DEBUG != 0))
    _trace_write_str ("FATAL: call to non implemented function.\n");
    _issue_break_point ();
#endif
    _exit (255);
    return 0;
}

// declare weak heap functions
void* __attribute__ ((weak, alias ("__nonimplemented_mem_cmd"))) _calloc_r (struct _reent* reent, unsigned num, unsigned sz);
void* __attribute__ ((weak, alias ("__nonimplemented_mem_cmd"))) _realloc_r (struct _reent* reent, void* ptr, unsigned sz);


//------------------------------------------------------------------------------
// implement newlib standard functions (weak)
//------------------------------------------------------------------------------

int __nonimplemented_syscall ()
{
    errno = ENOSYS;
    return -1;
}

int __attribute__ ((weak, alias ("__nonimplemented_syscall"))) _chown_r (struct _reent* reent, const char* path, uid_t owner, gid_t group);
int __attribute__ ((weak, alias ("__nonimplemented_syscall"))) _close_r (struct _reent* reent, int filedes);
int __attribute__ ((weak, alias ("__nonimplemented_syscall"))) _execve_r (struct _reent* reent, char* name, char** argv, char** env);
int __attribute__ ((weak, alias ("__nonimplemented_syscall"))) _fork_r (struct _reent* reent);
int __attribute__ ((weak, alias ("__nonimplemented_syscall"))) _fstat_r (struct _reent* reent, int fildes, struct stat* st);
int __attribute__ ((weak, alias ("__nonimplemented_syscall"))) _getpid_r (struct _reent* reent);
int __attribute__ ((weak, alias ("__nonimplemented_syscall"))) _gettimeofday_r (struct _reent* reent, struct timeval* ptimeval, void* ptimezone);
int __attribute__ ((weak, alias ("__nonimplemented_syscall"))) _isatty_r (struct _reent* reent, int file);
int __attribute__ ((weak, alias ("__nonimplemented_syscall"))) _kill_r (struct _reent* reent, int pid, int sig);
int __attribute__ ((weak, alias ("__nonimplemented_syscall"))) _link_r (struct _reent* reent, char* existing, char* _new);
int __attribute__ ((weak, alias ("__nonimplemented_syscall"))) _lseek_r (struct _reent* reent, int file, int ptr, int dir);
int __attribute__ ((weak, alias ("__nonimplemented_syscall"))) _open_r (struct _reent* reent, char* file, int flags, int mode);
int __attribute__ ((weak, alias ("__nonimplemented_syscall"))) _read_r (struct _reent* reent, int file, char* ptr, int len);
int __attribute__ ((weak, alias ("__nonimplemented_syscall"))) _readlink_r (struct _reent* reent, const char* path,  char* buf, size_t bufsize);
int __attribute__ ((weak, alias ("__nonimplemented_syscall"))) _stat_r (struct _reent* reent, const char* file,  struct stat* st);
int __attribute__ ((weak, alias ("__nonimplemented_syscall"))) _symlink_r (struct _reent* reent, const char* p1,  const char* p2);
int __attribute__ ((weak, alias ("__nonimplemented_syscall"))) _unlink_r (struct _reent* reent, char* status);
int __attribute__ ((weak, alias ("__nonimplemented_syscall"))) _wait_r (struct _reent* reent, int* status);
clock_t __attribute__((weak, alias ("__nonimplemented_syscall"))) _times_r (struct _reent* reent, struct tms* buf __attribute__((unused)));


//------------------------------------------------------------------------------
// implement default write function when using early debug
//------------------------------------------------------------------------------
#if 1
    #define dbgprint_buffer_size 32

    // implement write function weak
    int __attribute((weak)) _write_r (struct _reent* reent, int fd, char* ptr, int len)
    {
        char buffer[dbgprint_buffer_size];
        int left = len;
        while (left > 0)
        {
            int pos = 0;
            while ((pos < (dbgprint_buffer_size - 1)) && (left > 0))
            {
            char c = *(ptr++);
            left--;
            if (c == 0)
                break;
            buffer[pos++] = c;
            }
            buffer[pos] = '\0';
            _trace_write_str (buffer);
        }
        return len;
    }
#else
    int __attribute__ ((weak, alias ("__nonimplemented_syscall"))) _write_r (struct _reent* reent, int fd, char* ptr, int len);
#endif


} // extern "C"


//------------------------------------------------------------------------------
// override c++ new/delete operators (only if not using external heap module)
//------------------------------------------------------------------------------

#if (!defined(MODULE_HEAP_USED) || (MODULE_HEAP_USED == 0))

extern "C"
{
    // Heap locations - defined by linker
    extern unsigned int _Heap_Begin;
    extern unsigned int _Heap_Limit;
    extern int* _Heap_Pointer;

    // wrap memory allocation functions
    void* malloc (size_t sz)
    {
    sz = (sz + 3) >> 2;
    if ((_Heap_Pointer + sz) < (int*)(&_Heap_Limit))
    {
        void* ptr = _Heap_Pointer;
        _Heap_Pointer += sz;
        return ptr;
    }

    // assert here...
        return 0;
    }

    void free (void* ptr)
    {
    }

    // wrap memory allocation functions
    void* _malloc_r (struct _reent* reent, size_t sz)
    {
        return malloc (sz);
    }

    void __attribute__((weak, alias("free"))) _free_r (struct _reent* reent, void* ptr);

    // redirect default c++ operators
    void* __attribute((weak, alias("malloc"))) operator new (size_t sz);
    void* __attribute((weak, alias("malloc"))) operator new[] (size_t sz);
    void __attribute((weak, alias("free"))) operator delete (void* ptr);
    void __attribute((weak, alias("free"))) operator delete[] (void* ptr);
}

#else

    // delegate to heap module
    void* __attribute__ ((weak, alias ("__nonimplemented_mem_cmd"))) _malloc_r (struct _reent* reent, unsigned sz);
    void __attribute__ ((weak, alias ("__nonimplemented_mem_cmd"))) _free_r (struct _reent* reent, void* ptr);

#endif
