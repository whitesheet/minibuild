//==============================================================================
//
//   minibuild, a minimal bare-metal toolchain
//
//   License:       Copyright (c) 2018 Julian Dessecker
//
//                  Permission is hereby granted, free of charge, to any person
//                  obtaining a copy of this software and associated
//                  documentation files (the "Software"), to deal in the
//                  Software without restriction, including without limitation
//                  the rights to use, copy, modify, merge, publish, distribute,
//                  sublicense, and/or sell copies of the Software, and to
//                  permit persons to whom the Software is furnished to do so,
//                  subject to the following conditions:
//
//                  The above copyright notice and this permission notice shall
//                  be included in all copies or substantial portions of the
//                  Software.
//
//                  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
//                  KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//                  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//                  PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
//                  OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
//                  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
//                  OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
//                  THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//==============================================================================


#ifndef __ASSERT_H__
#define __ASSERT_H__


#ifdef __cplusplus
extern "C" {
#endif

#if (defined (DEBUG) && (DEBUG != 0))

// external assert failed implementation
extern void _assert_failed (const char* condition, const char* file, const char* line);

#define STRINGIFY_LINE_NO2(x) #x
#define STRINGIFY_LINE_NO(x) STRINGIFY_LINE_NO2(x)

// on debug builds, we want to have verbose assertions
#define assert(condition) {if (!(condition)) _assert_failed (#condition, __FILE__, STRINGIFY_LINE_NO(__LINE__));}

#else

// we do not use assertions on release builds
#define assert(condition)

#endif


#ifdef __cplusplus
} // extern "C"
#endif


#endif // __ASSERT_H__
