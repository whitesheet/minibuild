//==============================================================================
//
//   minibuild, a minimal bare-metal toolchain
//
//   License:       Copyright (c) 2018 Julian Dessecker
//
//                  Permission is hereby granted, free of charge, to any person
//                  obtaining a copy of this software and associated
//                  documentation files (the "Software"), to deal in the
//                  Software without restriction, including without limitation
//                  the rights to use, copy, modify, merge, publish, distribute,
//                  sublicense, and/or sell copies of the Software, and to
//                  permit persons to whom the Software is furnished to do so,
//                  subject to the following conditions:
//
//                  The above copyright notice and this permission notice shall
//                  be included in all copies or substantial portions of the
//                  Software.
//
//                  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
//                  KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//                  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//                  PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
//                  OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
//                  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
//                  OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
//                  THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//==============================================================================


#ifndef __CORE_CLIB_STUBS_H__
#define __CORE_CLIB_STUBS_H__


#include<sys/reent.h>
#if MODULE_HEAP_USED
#   include<heap.h>
#endif


#ifdef __cplusplus
extern "C" {
#endif


//------------------------------------------------------------------------------
// reentrant data structure
//------------------------------------------------------------------------------
struct __newlib_reent_t
{
    struct _reent reent;

#   if MODULE_HEAP_USED
        struct __HeapT* heap;
#   endif
};

#if MODULE_HEAP_USED
    extern struct __HeapT* __default_heap;
#endif


//------------------------------------------------------------------------------
// define default reentrant data structure
//------------------------------------------------------------------------------
#if MODULE_HEAP_USED
#   define DECLARE_REENT_STRUCT(name) struct __newlib_reent_t name = \
        {_REENT_INIT(name.reent), 0}
#else
#   define DECLARE_REENT_STRUCT(name) struct __newlib_reent_t name = \
        {_REENT_INIT(name.reent)}
#endif


//------------------------------------------------------------------------------
// get thread local data reference
//------------------------------------------------------------------------------
#define __reent_get() ((struct __newlib_reent_t*) _impure_ptr)


//------------------------------------------------------------------------------
// set thread local data reference
//------------------------------------------------------------------------------
#define __reent_set(ptr) *((struct __newlib_reent_t**) &_impure_ptr)=ptr;


#ifdef __cplusplus
} // extern "C"
#endif


#endif // __CORE_CLIB_STUBS_H__
