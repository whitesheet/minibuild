#!/bin/bash

#===============================================================================
#
#    minibuild, a minimal bare-metal toolchain
#
#    License:       Copyright (c) 2018 Julian Dessecker
#
#                   Permission is hereby granted, free of charge, to any person
#                   obtaining a copy of this software and associated
#                   documentation files (the "Software"), to deal in the
#                   Software without restriction, including without limitation
#                   the rights to use, copy, modify, merge, publish, distribute,
#                   sublicense, and/or sell copies of the Software, and to
#                   permit persons to whom the Software is furnished to do so,
#                   subject to the following conditions:
#
#                   The above copyright notice and this permission notice shall
#                   be included in all copies or substantial portions of the
#                   Software.
#
#                   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
#                   KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
#                   WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
#                   PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
#                   OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
#                   OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
#                   OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
#                   THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
#===============================================================================

# add baremetal arm cmake files
OPTS="$MODULE_PATH/../toolchain-_baremetal-arm-cmake"
source minibuild.source_module

# install minibuild elf info implementation
export MBTC_ELF_INFO_IMPL="$MODULE_PATH/scripts/minibuild.elf_info.impl"

# export toolchain binaries
export MBTC_AR="$MINIBUILD_ROOT/bin/arm-none-eabi-ranlib"
export MBTC_AR="$MINIBUILD_ROOT/bin/arm-none-eabi-ar"
export MBTC_AS="$MINIBUILD_ROOT/bin/arm-none-eabi-as"
export MBTC_CC="$MINIBUILD_ROOT/bin/arm-none-eabi-gcc"
export MBTC_CXX="$MINIBUILD_ROOT/bin/arm-none-eabi-g++"
export MBTC_GDB="$MINIBUILD_ROOT/bin/arm-none-eabi-gdb"
export MBTC_LD="$MINIBUILD_ROOT/bin/arm-none-eabi-ld"
export MBTC_XLD="$MINIBUILD_ROOT/bin/arm-none-eabi-g++"
export MBTC_NM="$MINIBUILD_ROOT/bin/arm-none-eabi-nm"
export MBTC_OBJCOPY="$MINIBUILD_ROOT/bin/arm-none-eabi-objcopy"
export MBTC_OBJDUMP="$MINIBUILD_ROOT/bin/arm-none-eabi-objdump"
export MBTC_SIZE="$MINIBUILD_ROOT/bin/arm-none-eabi-size"
export MBTC_STRIP="$MINIBUILD_ROOT/bin/arm-none-eabi-strip"

# default toolchain config, for all languages
MBTT_ASFLAGS=""
MBTT_CCFLAGS=""
MBTT_CXXFLAGS=""
MBTT_LDFLAGS=""
MBTT_XLDFLAGS=""

# enable inline assembler for gcc
export COMPILER_SUPPORTS_INLINE_ASM=1

# toolchain config for gcc
export MBTC_GCC_ASFLAGS="--defsym TOOLCHAIN_SUPPORTS_INLINE_ASM=1"
export MBTC_GCC_CCFLAGS="-DTOOLCHAIN_SUPPORTS_INLINE_ASM=1"
export MBTC_GCC_CXXFLAGS="-DTOOLCHAIN_SUPPORTS_INLINE_ASM=1 -std=c++11"
export MBTC_GCC_LDFLAGS=""
export MBTC_GCC_XLDFLAGS="-DTOOLCHAIN_SUPPORTS_INLINE_ASM=1"

# default target configurations
MBTT_R_RELEASE="-Os -g3"
MBTT_R_DEBUG="-Og -g3 -DDEBUG=1"
MBTT_R_MINSIZEREL="-Os -g3"
MBTT_R_RELWITHDEBINFO="-Os -g3"

# export basic toolchain settings
export MBTC_ASFLAGS_RELEASE="$MBTT_ASFLAGS"
export MBTC_ASFLAGS_DEBUG="$MBTT_ASFLAGS"
export MBTC_ASFLAGS_MINSIZEREL="$MBTT_ASFLAGS"
export MBTC_ASFLAGS_RELWITHDEBINFO="$MBTT_ASFLAGS"
export MBTC_CCFLAGS_RELEASE="$MBTT_CCFLAGS${MBTT_R_RELEASE}"
export MBTC_CCFLAGS_DEBUG="$MBTT_CCFLAGS${MBTT_R_DEBUG} -fno-lto"
export MBTC_CCFLAGS_MINSIZEREL="$MBTT_CCFLAGS${MBTT_R_MINSIZEREL}"
export MBTC_CCFLAGS_RELWITHDEBINFO="$MBTT_CCFLAGS${MBTT_R_RELWITHDEBINFO}"
export MBTC_CXXFLAGS_RELEASE="$MBTT_CXXFLAGS${MBTT_R_RELEASE}"
export MBTC_CXXFLAGS_DEBUG="$MBTT_CXXFLAGS${MBTT_R_DEBUG} -fno-lto"
export MBTC_CXXFLAGS_MINSIZEREL="$MBTT_CXXFLAGS${MBTT_R_MINSIZEREL}"
export MBTC_CXXFLAGS_RELWITHDEBINFO="$MBTT_CXXFLAGS${MBTT_R_RELWITHDEBINFO}"
export MBTC_LDFLAGS_RELEASE="$MBTT_LDFLAG${MBTT_R_RELEASE}"
export MBTC_LDFLAGS_DEBUG="$MBTT_LDFLAGS${MBTT_R_DEBUG} -fno-lto"
export MBTC_LDFLAGS_MINSIZEREL="$MBTT_LDFLAGS${MBTT_R_MINSIZEREL}"
export MBTC_LDFLAGS_RELWITHDEBINFO="$MBTT_LDFLAGS${MBTT_R_RELWITHDEBINFO}"
export MBTC_XLDFLAGS_RELEASE="$MBTT_XLDFLAGS${MBTT_R_RELEASE}"
export MBTC_XLDFLAGS_DEBUG="$MBTT_XLDFLAGS${MBTT_R_DEBUG} -fno-lto"
export MBTC_XLDFLAGS_MINSIZEREL="$MBTT_XLDFLAGS${MBTT_R_MINSIZEREL}"
export MBTC_XLDFLAGS_RELWITHDEBINFO="$MBTT_XLDFLAGS${MBTT_R_RELWITHDEBINFO}"

# get default includes from toolchain
MBTC_TEMP_CC_FEATURE_DETECT=`mktemp`
MBTC_TEMP_CXX_FEATURE_DETECT=`mktemp`
echo "" | $MBTC_CC -xc -E -v - 1> /dev/null 2> $MBTC_TEMP_CC_FEATURE_DETECT
echo "" | $MBTC_CC -xc++ -E -v - 1> /dev/null 2> $MBTC_TEMP_CXX_FEATURE_DETECT
export MBTC_CC_DEFAULT_INCLUDES=`cat $MBTC_TEMP_CC_FEATURE_DETECT | awk '/include <...> search starts here:/{f=1} /End of search list./{f=0;print} f' | grep -v 'include <...> search starts here' | grep -v 'End of search list.' | sed 's/^ *//;s/ *$//' | tr '\n' ':' | sed 's/:*$//g'`
export MBTC_CC_DEFAULT_LIBPATH=`cat $MBTC_TEMP_CC_FEATURE_DETECT | grep LIBRARY_PATH= | cut -d= -f2-`
export MBTC_CXX_DEFAULT_INCLUDES=`cat $MBTC_TEMP_CXX_FEATURE_DETECT | awk '/include <...> search starts here:/{f=1} /End of search list./{f=0;print} f' | grep -v 'include <...> search starts here' | grep -v 'End of search list.' | sed 's/^ *//;s/ *$//' | tr '\n' ':' | sed 's/:*$//g'`
export MBTC_CXX_DEFAULT_LIBPATH=`cat $MBTC_TEMP_CXX_FEATURE_DETECT | grep LIBRARY_PATH= | cut -d= -f2-`
rm $MBTC_TEMP_CC_FEATURE_DETECT
rm $MBTC_TEMP_CXX_FEATURE_DETECT
