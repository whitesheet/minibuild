#!/bin/bash

# supported view types
SUPPORTED_VIEWS="size size_dec"

# list view types?
if [ $# -eq 1 ]; then
	if [ "x$1" == "xlist-views" ]; then
		echo $SUPPORTED_VIEWS | sed 's/ /\n/g'
		exit 0
	fi
fi

# need to print help ?
if [ $# -ne 2 ]; then
	echo "usage: $0 [$SUPPORTED_VIEWS] <elf-file>"
	exit 0
fi

# check for valid view
CHECK_VIEW=`echo " $SUPPORTED_VIEWS " | grep " $1 "`
if [ "x$CHECK_VIEW" == "x" ]; then
	echo "invalid view: $1, supported views: $SUPPORTED_VIEWS"
	exit 1
fi


# abort on error
set -e

# get sections from elf file
TEMPFILE=`mktemp`
$MBTC_OBJDUMP -h $2 | sed 's/  */ /g' | grep '^ ' | while read line
do
	FLIST_ITEM=`echo "$line" | cut -d' ' -f1`
	if [[ $FLIST_ITEM =~ ^-?[0-9]+$ ]]; then
		FLIST_ITEM=`echo "$line" | cut -d' ' -f2-`
		echo -n "$FLIST_ITEM" >> $TEMPFILE
	else
		echo " $line" >> $TEMPFILE
	fi
done

# get size as hex number
function elfinfo_fmt_size
{
	SECTION_SIZE_HEX=`echo "$1" | cut -d' ' -f2`
	echo $SECTION_SIZE_HEX
}

# get size as decimal number
function elfinfo_fmt_size_dec
{
	SECTION_SIZE_HEX=`echo "$1" | cut -d' ' -f2`
	echo $(( 16#$SECTION_SIZE_HEX ))
}

# print sections
elf_info_functor="elfinfo_fmt_$1"
cat $TEMPFILE | while read section
do
	SECTION_NAME=`echo $section | cut -d' ' -f1`
	SECTION_VAL=`$elf_info_functor "$section"`
	echo "$SECTION_NAME $SECTION_VAL"
done

# remove temporary section file
rm $TEMPFILE
