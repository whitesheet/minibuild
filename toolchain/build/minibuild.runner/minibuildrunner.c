#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string.h>
#include <string>
#include <unistd.h>

// empty string
static char emptyStr[] = {0};

// copy argument
char* copyArg (const char* arg)
{
    if (!arg)
    return NULL;
    int sz = strlen (arg);
    char* cp = (char*) malloc (sz + 1);
    memcpy (cp, arg, sz + 1);
    return cp;
}

// get environment variable from name
char* getenv_log (FILE* log, const char* name)
{
    // get env parameter
    char* p = getenv (name);
    if (p == NULL)
    p = emptyStr;
    if (strlen (p) == 0)
    p = emptyStr;
    fprintf (log, "%s=%s\n", name, p ? p : "");

    // escape parameter
    if (p)
    {
        std::string sb;
        int sl = strlen (p);
    for (int i=0; i<sl; i++)
    {
        char c = p[i];
        if (c == '\"')
        {
        sb += "\\\"";
        }
        else
        {
        char str[2] = {c, 0};
        sb += str;
        }
    }
    return copyArg (sb.c_str ());
    }

    // return null
    return NULL;
}

// invoke configure script
std::vector<std::string> invoke_config_script (FILE* log, const char* pTarget, const char* pToolchain, const char* pModules, const char* pConfiguration, const char* pFlags)
{
    std::vector<std::string> lines;
    char cmdline[32768];
#if WRAPPER_FOR_GDB
    sprintf (cmdline, "/bin/bash -c \"TARGET=%s TOOLCHAIN=%s MODULES=%s CONFIGURATION=%s FLAGS=%s source minibuild 1> /dev/null ; minibuild.debug gdb\"", pTarget, pToolchain, pModules, pConfiguration, pFlags);
#elif WRAPPER_FOR_OPENOCD
    sprintf (cmdline, "/bin/bash -c \"TARGET=%s TOOLCHAIN=%s MODULES=%s CONFIGURATION=%s FLAGS=%s source minibuild 1> /dev/null ; minibuild.debug openocd\"", pTarget, pToolchain, pModules, pConfiguration, pFlags);
#else
#   error wrapper type not defined
#endif
    fprintf (log, "\nconfig commandline: '%s'\n", cmdline);

    FILE* fp = popen (cmdline, "r");
    if (fp)
    {
        char buffer[32768];
        while (fgets (buffer, sizeof (buffer) - 1, fp) != NULL)
        {
        // trim trailling newline
        int sl = strlen (buffer);
        if (sl > 0)
        {
        if (buffer[sl-1] == '\n')
            buffer[sl-1] = 0;
        }
        lines.push_back (buffer);
        }
    pclose (fp);
    }
    return lines;
}

// invoke openocd after evaluating toolchain based settings
int main (int argc, char** argv)
{

    // log invocation arguments
    FILE* log = fopen ("/tmp/openocd.log", "w");
    fprintf (log, "started with the following arguments:\n");
    for (int i=1; i<argc; i++)
    {
    fprintf (log, "%s\n", argv[i]);
    }
    fprintf (log, "\n");

    // get debugger arguments from environment
    char* pTOOLCHAIN = getenv_log (log, "TOOLCHAIN");
    char* pTARGET = getenv_log (log, "TARGET");
    char* pCONFIGURATION = getenv_log (log, "CONFIGURATION");
    char* pMODULES = getenv_log (log, "MODULES");
    char* pFLAGS = getenv_log (log, "FLAGS");

    // config script output
    fprintf (log, "\noutput from config script:\n");
    std::vector<std::string> lines = invoke_config_script (log, pTARGET, pTOOLCHAIN, pMODULES, pCONFIGURATION, pFLAGS);
    for (auto l: lines)
    {
    fprintf (log, "OUT: %s\n", l.c_str ());
    }

    // check for valid executable
    if (lines.empty ())
    {
    fprintf (stderr, "not able to detect gdb binary\n");
    }

    // create command line arguments
    std::vector<char*> arguments;
    for (int i=1; i<lines.size (); i++)
    {
        arguments.push_back (copyArg (lines[i].c_str ()));
    }
    for (int i=1; i<argc; i++)
        arguments.push_back (copyArg (argv[i]));
    arguments.push_back (NULL);

    fclose (log);

    // exec openocd
    execv (lines[0].c_str (), (char**) &arguments[0]);
    exit (127);
}
