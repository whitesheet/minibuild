#!/bin/bash

#===============================================================================
#
#    minibuild, a minimal bare-metal toolchain
#
#    License:       Copyright (c) 2018 Julian Dessecker
#
#                   Permission is hereby granted, free of charge, to any person
#                   obtaining a copy of this software and associated
#                   documentation files (the "Software"), to deal in the
#                   Software without restriction, including without limitation
#                   the rights to use, copy, modify, merge, publish, distribute,
#                   sublicense, and/or sell copies of the Software, and to
#                   permit persons to whom the Software is furnished to do so,
#                   subject to the following conditions:
#
#                   The above copyright notice and this permission notice shall
#                   be included in all copies or substantial portions of the
#                   Software.
#
#                   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
#                   KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
#                   WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
#                   PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
#                   OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
#                   OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
#                   OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
#                   THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
#===============================================================================

# abort on error
set -e

# get own script root
SCRIPT_ROOT=`readlink -f $0`
SCRIPT_ROOT=`dirname $SCRIPT_ROOT`
cd $SCRIPT_ROOT


# get host target triplet
HOSTTARGET=`$SCRIPT_ROOT/../../../scripts/hosttarget.sh`
if [ "x$HOSTTARGET" == "x" ]; then
    echo "Could not get host target triplet."
    exit 1
fi

# setup installation directory
INSTALL_DIR=$SCRIPT_ROOT/../../install
MOD_INSTALL_DIR=$INSTALL_DIR/minibuild.runner
MOD_INSTALL_DIR_TEMP=$MOD_INSTALL_DIR.temp
INSTALL_PACK=$SCRIPT_ROOT/../../repo/minibuild.runner-prebuild-$HOSTTARGET.tar.bz2
TEMP_DIR=$SCRIPT_ROOT/../../temp

# abort if package is already build
if [ -d $SCRIPT_ROOT/../../repo ]; then
    if [ -f $INSTALL_PACK ]; then
        echo "minibuild.runner already packaged"
        exit 0
    fi
fi

# create temp directory
if [ -d $TEMP_DIR ]; then
    echo "using already existing temp dir"
else
    mkdir $TEMP_DIR
fi

# decompress make sources
if [ -d $TEMP_DIR/minibuild.runner ]; then
    rm -rf $TEMP_DIR/minibuild.runner
fi
mkdir -p $TEMP_DIR/minibuild.runner
cd $TEMP_DIR/minibuild.runner
cp $SCRIPT_ROOT/*.c .

# build wrappers
g++ -DWRAPPER_FOR_GDB=1 --std=c++11 minibuildrunner.c -o minibuild.run-gdb
g++ -DWRAPPER_FOR_OPENOCD=1 --std=c++11 minibuildrunner.c -o minibuild.run-openocd

# install minibuild
if [ -d $MOD_INSTALL_DIR_TEMP ]; then
    rm -rf $MOD_INSTALL_DIR_TEMP
fi
mkdir -p $MOD_INSTALL_DIR_TEMP
cd $MOD_INSTALL_DIR_TEMP
cp $TEMP_DIR/minibuild.runner/minibuild.run-gdb .
cp $TEMP_DIR/minibuild.runner/minibuild.run-openocd .

# package make
mv $MOD_INSTALL_DIR_TEMP $MOD_INSTALL_DIR
cd $MOD_INSTALL_DIR
tar -cjf $INSTALL_PACK.temp *
mv $INSTALL_PACK.temp $INSTALL_PACK

# remove install and temp directory
rm -rf $SCRIPT_ROOT/../../temp/minibuild.runner
rm -rf $MOD_INSTALL_DIR

# we are done
echo
echo "minibuild.runner was build successfully."
