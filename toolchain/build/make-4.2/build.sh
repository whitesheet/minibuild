#!/bin/bash

#===============================================================================
#
#    minibuild, a minimal bare-metal toolchain
#
#    License:       Copyright (c) 2018 Julian Dessecker
#
#                   Permission is hereby granted, free of charge, to any person
#                   obtaining a copy of this software and associated
#                   documentation files (the "Software"), to deal in the
#                   Software without restriction, including without limitation
#                   the rights to use, copy, modify, merge, publish, distribute,
#                   sublicense, and/or sell copies of the Software, and to
#                   permit persons to whom the Software is furnished to do so,
#                   subject to the following conditions:
#
#                   The above copyright notice and this permission notice shall
#                   be included in all copies or substantial portions of the
#                   Software.
#
#                   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
#                   KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
#                   WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
#                   PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
#                   OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
#                   OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
#                   OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
#                   THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
#===============================================================================

# abort on error
set -e

# get own script root
SCRIPT_ROOT=`readlink -f $0`
SCRIPT_ROOT=`dirname $SCRIPT_ROOT`
cd $SCRIPT_ROOT

# get host target triplet
HOSTTARGET=`$SCRIPT_ROOT/../../../scripts/hosttarget.sh`
if [ "x$HOSTTARGET" == "x" ]; then
    echo "Could not get host target triplet."
    exit 1
fi

# setup installation directory
INSTALL_DIR=$SCRIPT_ROOT/../../install
MOD_INSTALL_DIR=$INSTALL_DIR/make-4.2
MOD_INSTALL_DIR_TEMP=$MOD_INSTALL_DIR.temp
INSTALL_PACK=$SCRIPT_ROOT/../../repo/make-4.2-prebuild-$HOSTTARGET.tar.bz2
TEMP_DIR=$SCRIPT_ROOT/../../temp

# abort if package is already build
if [ -d $SCRIPT_ROOT/../../repo ]; then
    if [ -f $INSTALL_PACK ]; then
        echo "make-4.2 already packaged"
        exit 0
    fi
fi

# create temp directory
if [ -d $TEMP_DIR ]; then
    echo "using already existing temp dir"
else
    mkdir $TEMP_DIR
fi

# try to download sources if not present
./download.sh

# decompress make sources
echo "decompressing make-4.2 sources..."
cd $TEMP_DIR
if [ -d "make-4.2.src" ]; then
    rm -rf make-4.2.src
fi
tar -xJf $SCRIPT_ROOT/../../repo/make-4.2.src.tar.xz
cd make-4.2

# configure make
./configure --prefix=$MOD_INSTALL_DIR_TEMP

# make make :-)
make

# install make
if [ -d $MOD_INSTALL_DIR_TEMP ]; then
    rm -rf $MOD_INSTALL_DIR_TEMP
fi
mkdir -p $MOD_INSTALL_DIR_TEMP
cd $TEMP_DIR/make-4.2
make install
mv $MOD_INSTALL_DIR_TEMP $MOD_INSTALL_DIR

# package make
cd $MOD_INSTALL_DIR
tar -cjf $INSTALL_PACK.temp *
mv $INSTALL_PACK.temp $INSTALL_PACK

# remove install and temp directory
rm -rf $SCRIPT_ROOT/../../temp/make-4.2
rm -rf $MOD_INSTALL_DIR

# we are done
echo
echo "make-4.2 was build successfully."
