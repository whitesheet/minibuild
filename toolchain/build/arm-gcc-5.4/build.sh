#!/bin/bash

#===============================================================================
#
#    minibuild, a minimal bare-metal toolchain
#
#    License:       Copyright (c) 2018 Julian Dessecker
#
#                   Permission is hereby granted, free of charge, to any person
#                   obtaining a copy of this software and associated
#                   documentation files (the "Software"), to deal in the
#                   Software without restriction, including without limitation
#                   the rights to use, copy, modify, merge, publish, distribute,
#                   sublicense, and/or sell copies of the Software, and to
#                   permit persons to whom the Software is furnished to do so,
#                   subject to the following conditions:
#
#                   The above copyright notice and this permission notice shall
#                   be included in all copies or substantial portions of the
#                   Software.
#
#                   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
#                   KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
#                   WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
#                   PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
#                   OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
#                   OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
#                   OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
#                   THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
#===============================================================================

# abort on error
set -e

# get own script root
SCRIPT_ROOT=`readlink -f $0`
SCRIPT_ROOT=`dirname $SCRIPT_ROOT`
cd $SCRIPT_ROOT

# get host target triplet
HOSTTARGET=`$SCRIPT_ROOT/../../../scripts/hosttarget.sh`
if [ "x$HOSTTARGET" == "x" ]; then
    echo "Could not get host target triplet."
    exit 1
fi

# setup installation directory
INSTALL_DIR=$SCRIPT_ROOT/../../install
MOD_INSTALL_DIR=$INSTALL_DIR/gcc-arm-none-eabi-5.4
MOD_INSTALL_DIR_TEMP=$INSTALL_DIR/gcc-arm-none-eabi-5.4.temp
INSTALL_PACK=$SCRIPT_ROOT/../../repo/arm-gcc-5.4-prebuild-$HOSTTARGET.tar.bz2

# abort if package is already build
if [ -d $SCRIPT_ROOT/../../repo ]; then
    if [ -f $INSTALL_PACK ]; then
        echo "gcc-arm-none-eabi-5.4 already packaged"
        exit 0
    fi
fi

# create new temp directory
if [ -d "../../temp" ]; then
    echo "using already existing temp dir"
else
    mkdir ../../temp
fi

# try to download sources if not present
./download.sh

# decompress toolchain
echo "decompressing arm-gcc-5.4 tool chain..."
cd ../../temp
if [ -d arm-gcc-5_4 ]; then
    rm -rf arm-gcc-5_4
fi
tar -xf ../repo/gcc-arm-none-eabi-5_4.src.tar.xz
mv gcc-arm-none-eabi-5_4-2016q3-20160926 arm-gcc-5_4
cd arm-gcc-5_4
echo

# decompress tars in src dir
echo "decompressing source packets..."
cd src
find -name '*.tar.*' | xargs -I% tar -xf %
cd ..
echo

# patch in custom toolchain options
echo "patching tool chain settings"
patch -p1 < $SCRIPT_ROOT/customopts.patch
echo

# build tool chain
echo "compiling prerequisites"
./build-prerequisites.sh --skip_steps=mingw32
echo

echo "compiling toolchain"
./build-toolchain.sh --skip_steps=mingw32,manual,mingw32-gdb-with-python
echo

# install toolchain
cd ..
if [ -d "$MOD_INSTALL_DIR_TEMP" ]; then
    rm -rf "$MOD_INSTALL_DIR_TEMP"
fi
if [ -d "$MOD_INSTALL_DIR" ]; then
    rm -rf "$MOD_INSTALL_DIR"
fi
mkdir -p "$MOD_INSTALL_DIR_TEMP"
cp -rf ./arm-gcc-5_4/install-native/* $MOD_INSTALL_DIR_TEMP
mv $MOD_INSTALL_DIR_TEMP $MOD_INSTALL_DIR

# package toolchain
cd $MOD_INSTALL_DIR
tar -cjf $INSTALL_PACK.temp *
mv $INSTALL_PACK.temp $INSTALL_PACK

# remove temp & install directory
cd $SCRIPT_ROOT
rm -rf /../../temp
rm -rf $MOD_INSTALL_DIR

# we are done
echo
echo "arm-gcc-5.4 toolchain was build successfully."
