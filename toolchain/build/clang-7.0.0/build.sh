#!/bin/bash

#===============================================================================
#
#    minibuild, a minimal bare-metal toolchain
#
#    License:       Copyright (c) 2018 Julian Dessecker
#
#                   Permission is hereby granted, free of charge, to any person
#                   obtaining a copy of this software and associated
#                   documentation files (the "Software"), to deal in the
#                   Software without restriction, including without limitation
#                   the rights to use, copy, modify, merge, publish, distribute,
#                   sublicense, and/or sell copies of the Software, and to
#                   permit persons to whom the Software is furnished to do so,
#                   subject to the following conditions:
#
#                   The above copyright notice and this permission notice shall
#                   be included in all copies or substantial portions of the
#                   Software.
#
#                   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
#                   KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
#                   WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
#                   PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
#                   OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
#                   OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
#                   OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
#                   THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
#===============================================================================

# abort on error
set -e

# select optional clang features
CLANG_EXTRA_TOOLS="0"
CLANG_COMPILER_RT="0"
CLANG_COMPILER_LIBCXX="0"
CLANG_COMPILER_LIBCXXABI="0"
CLANG_COMPILER_LIBUNWIND="0"

# get own script root
SCRIPT_ROOT=`readlink -f $0`
SCRIPT_ROOT=`dirname $SCRIPT_ROOT`
cd $SCRIPT_ROOT

# get host target triplet
HOSTTARGET=`$SCRIPT_ROOT/../../../scripts/hosttarget.sh`
if [ "x$HOSTTARGET" == "x" ]; then
    echo "Could not get host target triplet."
    exit 1
fi

# setup installation directory
INSTALL_DIR=$SCRIPT_ROOT/../../install
MOD_INSTALL_DIR=$INSTALL_DIR/clang-7.0.0
MOD_INSTALL_DIR_TEMP=$INSTALL_DIR/clang-7.0.0.temp
INSTALL_PACK=$SCRIPT_ROOT/../../repo/clang-7.0.0-prebuild-$HOSTTARGET.tar.bz2
TEMP_DIR=$SCRIPT_ROOT/../../temp

# abort if package is already build
if [ -d $SCRIPT_ROOT/../../repo ]; then
    if [ -f $INSTALL_PACK ]; then
        echo "clang-7.0.0 already packaged"
        exit 0
    fi
fi

# clean up temp directory
if [ -d $TEMP_DIR ]; then
    echo "using already existing temp dir"
else
    mkdir $TEMP_DIR
fi

# try to download sources if not present
./download.sh

# decompress llvm
echo "decompressing clang-7.0.0 tool chain..."
cd $TEMP_DIR
if [ -d "llvm-7.0.0.src" ]; then
    rm -rf llvm-7.0.0.src
fi
tar -xf $SCRIPT_ROOT/../../repo/llvm-7.0.0.src.tar.xz
cd llvm-7.0.0.src
cd tools

# decompress clang
echo "decompressing clang-7.0.0 sources..."
tar -xJf $SCRIPT_ROOT/../../repo/cfe-7.0.0.src.tar.xz
mv cfe-7.0.0.src clang
cd clang

# decompress clang extra tools
if [ "x$CLANG_EXTRA_TOOLS" == "x1" ]; then
    echo "decompressing clang-7.0.0 extra tool sources..."
    cd tools
    tar -xJf $SCRIPT_ROOT/../../repo/clang-tools-extra-7.0.0.src.tar.xz
    mv clang-tools-extra-7.0.0.src extra
fi

# decompress compiler-rt
if [ "x1" == "x$CLANG_COMPILER_RT" ]; then
    echo "decompressing clang-7.0.0 compiler-rt sources..."
    cd $TEMP_DIR/llvm-7.0.0.src/projects
    tar -xJf $SCRIPT_ROOT/../../repo/compiler-rt-7.0.0.src.tar.xz
    mv compiler-rt-7.0.0.src compiler-rt
fi

# decompress lld
echo "decompressing lld-7.0.0 sources..."
cd $TEMP_DIR/llvm-7.0.0.src/tools
tar -xJf $SCRIPT_ROOT/../../repo/lld-7.0.0.src.tar.xz
mv lld-7.0.0.src lld

# decompress lldb
echo "decompressing lldb-7.0.0 sources..."
cd $TEMP_DIR/llvm-7.0.0.src/tools
tar -xJf $SCRIPT_ROOT/../../repo/lldb-7.0.0.src.tar.xz
mv lldb-7.0.0.src lldb

# decompress libcxx
if [ "x1" == "x$CLANG_COMPILER_LIBCXX" ]; then
    echo "decompressing clang-7.0.0-libcxx sources..."
    cd $TEMP_DIR/llvm-7.0.0.src/projects
    tar -xJf $SCRIPT_ROOT/../../repo/libcxx-7.0.0.src.tar.xz
    mv libcxx-7.0.0.src libcxx
fi

# decompress libcxxabi
if [ "x1" == "x$CLANG_COMPILER_LIBCXXABI" ]; then
    echo "decompressing clang-7.0.0-libcxxabi sources..."
    cd $TEMP_DIR/llvm-7.0.0.src/projects
    tar -xJf $SCRIPT_ROOT/../../repo/libcxxabi-7.0.0.src.tar.xz
    mv libcxxabi-7.0.0.src libcxxabi
fi

# decompress libunwind
if [ "x1" == "x$CLANG_COMPILER_LIBUNWIND" ]; then
    echo "decompressing clang-7.0.0-libunwind sources..."
    cd $TEMP_DIR/llvm-7.0.0.src/runtimes
    tar -xJf $SCRIPT_ROOT/../../repo/libunwind-7.0.0.src.tar.xz
    mv libunwind-7.0.0.src libunwind
fi

# create out of tree build directory, delete old one if existing
cd $TEMP_DIR
if [ -d llvm-7.0.0.build ]; then
    rm -rf llvm-7.0.0.build
fi
mkdir llvm-7.0.0.build
cd llvm-7.0.0.build

# generate make scripts
CMAKEFLAGS="-DCMAKE_BUILD_TYPE=MinSizeRel -DLLVM_BUILD_TESTS=false -DLLVM_BUILD_LLVM_DYLIB=false -DBUILD_SHARED_LIBS=false -DCMAKE_INSTALL_PREFIX=$MOD_INSTALL_DIR_TEMP"
cmake $CMAKEFLAGS -G "Unix Makefiles" $TEMP_DIR/llvm-7.0.0.src

# build
make -j1

# install toolchain
if [ -d $MOD_INSTALL_DIR_TEMP ]; then
    rm -rf $MOD_INSTALL_DIR_TEMP
fi
if [ -d $MOD_INSTALL_DIR ]; then
    rm -rf $MOD_INSTALL_DIR
fi
mkdir -p $MOD_INSTALL_DIR_TEMP
make install
mv $MOD_INSTALL_DIR_TEMP $MOD_INSTALL_DIR

# package toolchain
cd $MOD_INSTALL_DIR
tar -cjf $INSTALL_PACK.temp *
mv $INSTALL_PACK.temp $INSTALL_PACK

# remove install and temp directory
if [ -d $SCRIPT_ROOT/../../temp/llvm-7.0.0.src ]; then
    rm -rf $SCRIPT_ROOT/../../temp/llvm-7.0.0.src
fi
if [ -d $SCRIPT_ROOT/../../temp/llvm-7.0.0.build ]; then
    rm -rf $SCRIPT_ROOT/../../temp/llvm-7.0.0.build
fi
rm -rf $MOD_INSTALL_DIR

# we are done
echo
echo "clang-7.0.0 toolchain was build successfully."
