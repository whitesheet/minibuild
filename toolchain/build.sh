#!/bin/bash

#===============================================================================
#
#    minibuild, a minimal bare-metal toolchain
#
#    License:       Copyright (c) 2018 Julian Dessecker
#
#                   Permission is hereby granted, free of charge, to any person
#                   obtaining a copy of this software and associated
#                   documentation files (the "Software"), to deal in the
#                   Software without restriction, including without limitation
#                   the rights to use, copy, modify, merge, publish, distribute,
#                   sublicense, and/or sell copies of the Software, and to
#                   permit persons to whom the Software is furnished to do so,
#                   subject to the following conditions:
#
#                   The above copyright notice and this permission notice shall
#                   be included in all copies or substantial portions of the
#                   Software.
#
#                   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
#                   KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
#                   WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
#                   PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
#                   OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
#                   OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
#                   OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
#                   THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
#===============================================================================

#abort on errors
set -e

# go to script root
SCRIPT_ROOT=`readlink -f $0`
SCRIPT_ROOT=`dirname $SCRIPT_ROOT`
cd $SCRIPT_ROOT
cd build

# build arm gcc 5.4
echo "Building arm-gcc-5.4 toolchain:"
cd $SCRIPT_ROOT/build/arm-gcc-5.4
./build.sh
cd ..

# build clang 7.0.0
echo "Building clang-7.0.0 toolchain:"
cd $SCRIPT_ROOT/build/clang-7.0.0
./build.sh
cd ..

# build cmake-3.12.2
echo "Building cmake-3.12.2:"
cd $SCRIPT_ROOT/build/cmake-3.12.2
./build.sh
cd ..

# build make-4.2
echo "Building make-4.2:"
cd $SCRIPT_ROOT/build/make-4.2
./build.sh
cd ..

# build openocd-0.10.0
echo "Building openocd-0.10.0:"
cd $SCRIPT_ROOT/build/openocd-0.10.0
./build.sh
cd ..

# minibuild.runner
echo "Building minibuild.runner:"
cd $SCRIPT_ROOT/build/minibuild.runner
./build.sh
cd ..
