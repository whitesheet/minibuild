#!/bin/bash

#===============================================================================
#
#    minibuild, a minimal bare-metal toolchain
#
#    License:       Copyright (c) 2018 Julian Dessecker
#
#                   Permission is hereby granted, free of charge, to any person
#                   obtaining a copy of this software and associated
#                   documentation files (the "Software"), to deal in the
#                   Software without restriction, including without limitation
#                   the rights to use, copy, modify, merge, publish, distribute,
#                   sublicense, and/or sell copies of the Software, and to
#                   permit persons to whom the Software is furnished to do so,
#                   subject to the following conditions:
#
#                   The above copyright notice and this permission notice shall
#                   be included in all copies or substantial portions of the
#                   Software.
#
#                   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
#                   KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
#                   WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
#                   PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
#                   OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
#                   OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
#                   OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
#                   THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
#===============================================================================

#abort on errors
set -e

# go to script root
SCRIPT_ROOT=`readlink -f $0`
SCRIPT_ROOT=`dirname $SCRIPT_ROOT`
cd $SCRIPT_ROOT

# get host target triplet
HOSTTARGET=`$SCRIPT_ROOT/scripts/hosttarget.sh`
if [ "x$HOSTTARGET" == "x" ]; then
    echo "Could not get host target triplet."
    exit 1
fi

# get version
# TODO: get from git repo
GIT_VERSION=`$SCRIPT_ROOT/scripts/minibuild.version`

# invoke child build scripts
./toolchain/download.sh
if [ "x$1" == "xdev" ]; then
    ./toolchain/download_dev.sh
fi
./toolchain/build.sh

# remove old minibuild installation
if [ -d $SCRIPT_ROOT/install ]; then
    rm -rf $SCRIPT_ROOT/install
fi

# create minibuild directory structure
mkdir -p $SCRIPT_ROOT/install
mkdir -p $SCRIPT_ROOT/install

# install arm-gcc-5.4 toolchain
echo "installing arm-gcc-5.4 toolchain..."
cd $SCRIPT_ROOT/install
tar -xjf $SCRIPT_ROOT/toolchain/repo/arm-gcc-5.4-prebuild-$HOSTTARGET.tar.bz2

# install clang-7.0.0 toolchain
echo "installing clang-7.0.0 toolchain..."
cd $SCRIPT_ROOT/install
tar -xjf $SCRIPT_ROOT/toolchain/repo/clang-7.0.0-prebuild-$HOSTTARGET.tar.bz2

# install cmake-3.12.2
echo "installing cmake-3.12.2..."
cd $SCRIPT_ROOT/install
tar -xjf $SCRIPT_ROOT/toolchain/repo/cmake-3.12.2-prebuild-$HOSTTARGET.tar.bz2

# install make-4.2
echo "installing make-4.2..."
cd $SCRIPT_ROOT/install
tar -xjf $SCRIPT_ROOT/toolchain/repo/make-4.2-prebuild-$HOSTTARGET.tar.bz2

# install openocd-0.10.0
echo "installing openocd-0.10.0..."
cd $SCRIPT_ROOT/install
tar -xjf $SCRIPT_ROOT/toolchain/repo/openocd-0.10.0-prebuild-$HOSTTARGET.tar.bz2

# install minibuild.runner
echo "installing minibuild.runner..."
cd $SCRIPT_ROOT/install
tar -xjf $SCRIPT_ROOT/toolchain/repo/minibuild.runner-prebuild-$HOSTTARGET.tar.bz2

# copy deployment folder into installation
echo "installing minibuild scripts..."
cd $SCRIPT_ROOT/install
cp -Lr $SCRIPT_ROOT/deploy/* $SCRIPT_ROOT/install
if [ "x$1" == "xdev" ]; then
    ln -s $SCRIPT_ROOT/content $SCRIPT_ROOT/install/content
else
    mkdir -p $SCRIPT_ROOT/install/content
    cp -Lr $SCRIPT_ROOT/content/* $SCRIPT_ROOT/install/content
fi

# link current version
if [ "x$1" == "xdev" ]; then
    echo -n ""
else
    echo "#!/bin/bash" > $SCRIPT_ROOT/install/content/sbin/minibuild.version
    echo "" >> $SCRIPT_ROOT/install/content/sbin/minibuild.version
    echo "echo \"${GIT_VERSION}\"" >> $SCRIPT_ROOT/install/content/sbin/minibuild.version
    chmod +x $SCRIPT_ROOT/install/content/sbin/minibuild.version
fi

# install examples
mkdir -p $SCRIPT_ROOT/install/share/minibuild
if [ "x$1" == "xdev" ]; then
    ln -s $SCRIPT_ROOT/examples $SCRIPT_ROOT/install/share/minibuild/examples
else
    cp -rf $SCRIPT_ROOT/examples $SCRIPT_ROOT/install/share/minibuild/examples
fi

# package default workspace
pushd $SCRIPT_ROOT/default-workspace
tar -cJf $SCRIPT_ROOT/install/share/minibuild/default-workspace.tar.xz .metadata
popd

# install version script
pushd $SCRIPT_ROOT/install
ln -s content/sbin/minibuild.version minibuild.version
popd

# apply patches to installation
cd $SCRIPT_ROOT/install
patch -p1 < ../patch/openocd.patch

# package tool chain
if [ "xdev" != "x$1" ]; then
    echo "packaging minibuild toolchain..."
    cd $SCRIPT_ROOT/install
    tar -cJf $SCRIPT_ROOT/minibuild-$GIT_VERSION-$HOSTTARGET.tar.xz.temp *
    mv $SCRIPT_ROOT/minibuild-$GIT_VERSION-$HOSTTARGET.tar.xz.temp $SCRIPT_ROOT/minibuild-$GIT_VERSION-$HOSTTARGET.tar.xz
fi

# set current build version
echo "$GIT_VERSION" > $SCRIPT_ROOT/minibuild-$HOSTTARGET.current_version
