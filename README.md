# Ready to use virtal machine

+ a ready to use virtual machine: [minibuild-v1.0.0.ova](https://drive.google.com/open?id=1wVUnolPpX_R0BVwe0lJUTF53HmeZWte8)
+ requires a virtual box 5.2.8 installation or newer: https://www.virtualbox.org/wiki/Downloads
+ requires the virtual box extension pack to be installed if you want to use USB debuggers